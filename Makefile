CXX = gcc

# Implicit rules needed to build .o files and Metaobject stuff (_m.o)
.SUFFIXES: .c .o .h

.c.o:
	$(CXX) -c $(XTRA_INCL)   $< -o $@
	
OBJECTS = daemonize.o 
daemonize: $(OBJECTS)
	$(CXX) -o $@ -pipe -O2 $(OBJECTS) $(RPATH_XT)
	strip --strip-unneeded $@

clean:
	@rm -f core *.o *_m.* *.bak *.elf
