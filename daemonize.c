#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>

int main(int argc, char *argv[]) {
	int i;
	// int reterr;
	pid_t pid, sid;
	
	//Fork the Parent Process
	pid = fork();
 
	if (pid < 0) { exit(EXIT_FAILURE); }
 
	//We got a good pid, Close the Parent Process
	if (pid > 0) { exit(EXIT_SUCCESS); }
 
	//Change File Mask
	umask(0);
 
	//Create a new Signature Id for our child
	sid = setsid();
	if (sid < 0) { exit(EXIT_FAILURE); }
 
	//Change Directory
	//If we cant find the directory we exit with failure.
	if ((chdir("/")) < 0) { exit(EXIT_FAILURE); }
 
	//Close Standard File Descriptors
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
 
	//----------------
	//Main Process
	//----------------
	for(i=0; i < argc - 1; i++) {
		argv[i]=argv[i+1];
	}
	argv[argc-1] = '\0';
	execv(argv[0], argv);
	//reterr = execv(argv[0], argv);
	//printf("execv failed with '%s'\n", strerror(errno));

	//Close the log
	closelog ();
}
